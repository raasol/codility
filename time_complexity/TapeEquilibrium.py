# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")


def solution(A):
    # write your code in Python 3.6
    minimum = None
    for i in range(1, len(A)):
        diff = abs(sum(A[:i]) - sum(A[i:]))
        minimum = diff if minimum is None or diff < minimum else minimum

    return minimum

def solutionB(A):
    s = sum(A)
    m = float('inf')
    left_sum = 0
    for i in A[:-1]:
        left_sum += i
        m = min(abs(s - 2*left_sum), m)
    return m
