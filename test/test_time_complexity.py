from . import data as d
from . import (
    TapeEquilibrium
)

data = d.data[__name__]

def test_tape_equilibrium():
    for d in data["test_tape_equilibrium"]:
        TapeEquilibrium.solution(d)